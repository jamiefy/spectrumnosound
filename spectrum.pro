include(spectrum.pri)

TEMPLATE = subdirs

# Ensure that library is built before application
CONFIG  += ordered

INCLUDEPATH += D:\2345Downloads\opencv3.2\opencv\build\include #*
               D:\2345Downloads\opencv3.2\opencv\build\include\opencv
               D:\2345Downloads\opencv3.2\opencv\build\include\opencv2
CONFIG += c++11
CONFIG(debug,debug|release):{
    LIBS += D:\2345Downloads\opencv3.2\opencv\build\lib\libopencv_*.a
 }else:CONFIG(release,debug|release):{
 LIBS += D:\opencv-4.0.1\buildopencv\lib\libopencv_*.a
 }

!contains(DEFINES, DISABLE_FFT): SUBDIRS += 3rdparty/fftreal
SUBDIRS += app

TARGET = spectrum

EXAMPLE_FILES += \
    README.txt \
    TODO.txt
